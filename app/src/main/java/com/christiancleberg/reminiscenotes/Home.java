package com.christiancleberg.reminiscenotes;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CreateNewNote.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // load files
        File folder = getFilesDir();
        File[] listOfFiles = folder.listFiles();
        int position = 0;

        // get username and location fields
        View header = navigationView.getHeaderView(0);
        LinearLayout sideNav = header.findViewById(R.id.sideNavLayout);
        TextView username = sideNav.findViewById(R.id.navUsername);
        TextView subtitle = sideNav.findViewById(R.id.navSubtitle);

        for (File file : listOfFiles) {
            if (file.isFile()) {
                position += 1;
                // ignore share history file
                if (file.getName().contains("custom_share_history")) {
                    continue;
                }
                // set username from name file
                else if (file.getName().contains("name.txt")) {
                    try {
                        InputStream in = openFileInput("name.txt");
                        if (in != null )
                        {
                            InputStreamReader tmp = new InputStreamReader(in);
                            BufferedReader reader = new BufferedReader(tmp);
                            String str;
                            StringBuilder buffer = new StringBuilder();
                            while ((str = reader.readLine()) != null )
                            {
                                buffer.append(str);
                            }
                            in.close();
                            username.setText(buffer.toString());
                        }
                    } catch (java.io.IOException e) {e.printStackTrace();}
                }
                // set user location from location file
                else if(file.getName().contains("location.txt")) {
                    try {
                        InputStream in = openFileInput("location.txt");
                        if (in != null )
                        {
                            InputStreamReader tmp = new InputStreamReader(in);
                            BufferedReader reader = new BufferedReader(tmp);
                            String str;
                            StringBuilder buffer = new StringBuilder();
                            while ((str = reader.readLine()) != null )
                            {
                                buffer.append(str);
                            }
                            in.close();
                            subtitle.setText(buffer.toString());
                        }
                    } catch (java.io.IOException e) {e.printStackTrace();}

                }
                // else, read files to create cards
                else {
                    Toast.makeText(this, "File: " + file.getName(), Toast.LENGTH_SHORT).show();
                    readFile(file.getName(), position);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            //TODO
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help) {
            Intent intent = new Intent(this, Help.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_share) {
            //TODO
            // share all note files?
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_delete_all) {
            deleteAllNotes();
            return true;
        } else if (id == R.id.nav_play_link) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.christiancleberg.reminisce"));
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_refresh) {
            refreshNotes();
            return true;
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------------
    private void createNewCard(List<String> stringList, int position) {
        LinearLayout notesLayout = findViewById(R.id.scrollLinearLayout);

        LayoutInflater inflater = getLayoutInflater();
        View newCard = inflater.inflate(R.layout.notecard, notesLayout, false);
        notesLayout.addView(newCard);

        final TextView title = findViewById(R.id.titleSpace);
        final TextView body = findViewById(R.id.bodySpace);
        final TextView location = findViewById(R.id.locationSpace);
        final TextView date = findViewById(R.id.dateSpace);

        newCard.setId(R.id.myNoteCard + position);
        title.setId(R.id.titleSpace + position);
        body.setId(R.id.bodySpace + position);
        location.setId(R.id.locationSpace + position);
        date.setId(R.id.dateSpace + position);

        title.setText(stringList.get(0));
        location.setText(stringList.get(1));
        date.setText(stringList.get(2));
        for (int i = 3; i < stringList.size(); i++) {
            if (!(i == stringList.size()-1)) {
                body.append(stringList.get(i) + "\n");
            } else {
                body.append(stringList.get(i));
            }
        }

        // Set onClickListener to allow user to re-open editing screen with the note's information.
        newCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Need to create temp file for "body" section - it could be too long/large to pass with .putExtra()
                try {
                    FileOutputStream fos = openFileOutput("body.temp", Context.MODE_PRIVATE);
                    fos.write(body.getText().toString().getBytes());
                    fos.flush();
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Pass everything else with Intent().putExtra()
                Intent i = new Intent(Home.this, CreateNewNote.class)
                        .putExtra("<Title>", title.getText())
                        .putExtra("<Location>", location.getText())
                        .putExtra("<Date>", date.getText());
                startActivity(i);
            }
        });
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public List<String> readFile(String file, int position) {
        List<String> lines = new ArrayList<String>();
        try {
            FileInputStream fin = openFileInput(file);
            InputStreamReader inputStream = new InputStreamReader(fin);
            BufferedReader bufferedReader = new BufferedReader(inputStream);
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            fin.close();
            inputStream.close();
            createNewCard(lines, position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void refreshNotes() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void deleteAllNotes() {
        File filesDir = getFilesDir();
        File fileList[] = filesDir.listFiles();
        for (File file : fileList) {
            if (file.isFile()) {
                if (file.getName().equals("name.txt") || file.getName().equals("location.txt")) {
                    continue;
                } else {
                    if (file.delete()) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        startActivity(new Intent(Home.this, Home.class));
                    } else {
                        Toast.makeText(this, getString(R.string.failedtoDeleteFiles), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }
}
