package com.christiancleberg.reminiscenotes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CreateNewNote extends AppCompatActivity {
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public android.support.v7.widget.ShareActionProvider mShareActionProvider;
    public final static String LOCATION = "location.txt";
    public static String[] notesArray;
    public TextView locationText;
    FloatingActionButton fab;
    EditText editTitle,editBody;
    TextView NewDate;
    // -----------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createnewnote);
        /* Display Location in TextView */
        getLocation();
        /* Get Date */
        Date currentTime = Calendar.getInstance().getTime();
        java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("MMM dd, hh:mm aa", Locale.US);
        String formattedCurrentDate = simpleDateFormat.format(currentTime);
        NewDate = findViewById(R.id.DateField);
        NewDate.setText(formattedCurrentDate);
        /* Save & Read Button Listeners */
        fab = findViewById(R.id.saveFAB);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createArray();
            }
        });
        /* Views for Save/Read Methods */
        editBody = findViewById(R.id.idBody);
        editTitle = findViewById(R.id.idName);
        /* Get the information if a note is clicked */
        getNotesIntent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.newnote, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.action_share);

        // Fetch and store ShareActionProvider
        try {
            mShareActionProvider = (android.support.v7.widget.ShareActionProvider) MenuItemCompat.getActionProvider(item);
            mShareActionProvider.setShareHistoryFileName("custom_share_history.xml");
        } catch(Exception e) {e.printStackTrace(); }

        return true;
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            /*
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom))
                    .setTitle(getString(R.string.deleteNote1))
                    .setMessage(getString(R.string.deleteNote2))
                    .setPositiveButton(getString(R.string.deleteNote3), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteNote();
                        }
                    })
                    .setNegativeButton(getString(R.string.deleteNote4), null).show();
                    */
            return true;
        } else if (id == R.id.action_indent) {
            editBody.append("     ");
            return true;
        } else if (id == R.id.action_bullet) {
            editBody.append("\u2022 ");
            return true;
        } else if (id == R.id.action_font) {
            switchFont();
            return true;
        } else if (id == R.id.action_share) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            String content = editTitle.getText().toString() + "\n\n" + editBody.getText().toString();
            shareIntent.putExtra(Intent.EXTRA_TEXT, content);
            shareIntent.setType("text/plain");
            setShareIntent(shareIntent);
            startActivity(shareIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void getNotesIntent() {
        try {
            // get body info from file
            String tempFile = "body.temp";
            String iBody = "";
            FileInputStream fis = openFileInput(tempFile);
            InputStreamReader inputStreamReader = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                iBody = iBody + line + "\n";
            }
            fis.close();
            inputStreamReader.close();
            // delete temp file after use
            File filesDir = getFilesDir();
            File fileList[] = filesDir.listFiles();
            for (File file : fileList) {
                if (file.getName().contains(tempFile)) {
                    try {
                        file.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            // get everything else
            String iTitle = getIntent().getStringExtra("<Title>");
            String iLocation = getIntent().getStringExtra("<Location>");
            String iDate = getIntent().getStringExtra("<Date>");
            // assign info to fields
            editTitle.setText(iTitle);
            locationText.setText(iLocation);
            NewDate.setText(iDate);
            editBody.setText(iBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void createArray() {
        notesArray = new String[]
                {
                        editTitle.getText().toString(),
                        locationText.getText().toString(),
                        NewDate.getText().toString(),
                        editBody.getText().toString()
                };
        try {
            String content = notesArray[0] + "\n" + notesArray[1] + "\n" + notesArray[2] + "\n" + notesArray[3];
            FileOutputStream fos = openFileOutput(notesArray[0] + "", Context.MODE_PRIVATE);
            fos.write(content.getBytes());
            fos.flush();
            fos.close();
            Toast.makeText(this, getString(R.string.dataSaved), Toast.LENGTH_SHORT).show();
        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.saveFailed), Toast.LENGTH_LONG).show();
        }
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void deleteNote() {
        String fileName = editTitle.getText().toString();
        File file = new File(getFilesDir() + "/" + fileName);
        if (file.delete()) {
            startActivity(new Intent(CreateNewNote.this, Home.class));
        } else {
            Toast.makeText(this, getString(R.string.failedtoDeleteFile), Toast.LENGTH_SHORT).show();
        }
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void getLocation() {
        locationText = findViewById(R.id.LocationField);
        // extract the location from file "LOCATION"
        try {
            InputStream in = openFileInput(LOCATION);
            if (in != null )
            {
                InputStreamReader tmp = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(tmp);
                String str;
                StringBuilder buffer = new StringBuilder();
                while ((str = reader.readLine()) != null )
                {
                    buffer.append(str);
                }
                in.close();
                locationText.setText(buffer.toString());
            }
        } catch (java.io.IOException e) {e.printStackTrace();}
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void switchFont() {
        if (editTitle.getTypeface() == Typeface.SANS_SERIF) {
            editTitle.setTypeface(Typeface.SERIF);
            editBody.setTypeface(Typeface.SERIF);
        } else {
            editTitle.setTypeface(Typeface.SANS_SERIF);
            editBody.setTypeface(Typeface.SANS_SERIF);
        }
    }
}