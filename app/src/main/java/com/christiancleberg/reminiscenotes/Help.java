package com.christiancleberg.reminiscenotes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class Help extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        // Enable web link.
        TextView textView = findViewById(R.id.AnswerThree);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

}
