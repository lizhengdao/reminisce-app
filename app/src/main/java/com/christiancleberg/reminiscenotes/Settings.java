package com.christiancleberg.reminiscenotes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Settings extends AppCompatActivity {
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public final static String LOCATION = "location.txt";
    public final static String NAME = "name.txt";
    public EditText locationText;
    public EditText nameText;
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        loadLocation();
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    public void saveSettings(View v)
    {
        /* save the activity_settings to load next time layout is opened */
        saveLocation();
        Toast.makeText(this,getString(R.string.settingsSaved),Toast.LENGTH_SHORT).show();
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    private void saveLocation()
    {
        locationText = findViewById(R.id.InputThree);
        nameText = findViewById(R.id.InputOne);
        try {
            OutputStreamWriter out = new OutputStreamWriter(openFileOutput(LOCATION, 0));
            out.write(locationText.getText().toString());
            out.close();
        } catch (java.io.IOException e) {e.printStackTrace();}
        try {
            OutputStreamWriter out = new OutputStreamWriter(openFileOutput(NAME, 0));
            out.write(nameText.getText().toString());
            out.close();
        } catch (java.io.IOException e) {e.printStackTrace();}
    }
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    private void loadLocation()
    {
        locationText = findViewById(R.id.InputThree);
        nameText = findViewById(R.id.InputOne);
        try {
            InputStream in = openFileInput(LOCATION);
            if (in != null )
            {
                InputStreamReader tmp = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(tmp);
                String str;
                StringBuilder buffer = new StringBuilder();
                while ((str = reader.readLine()) != null )
                {
                    buffer.append(str);
                }
                in.close();
                locationText.setText(buffer.toString());
            }
        } catch (java.io.IOException e) {e.printStackTrace();}
        try {
            InputStream in = openFileInput(NAME);
            if (in != null )
            {
                InputStreamReader tmp = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(tmp);
                String str;
                StringBuilder buffer = new StringBuilder();
                while ((str = reader.readLine()) != null )
                {
                    buffer.append(str);
                }
                in.close();
                nameText.setText(buffer.toString());
            }
        } catch (java.io.IOException e) {e.printStackTrace();}
    }
}