package com.christiancleberg.reminiscenotes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        // Enable web link.
        TextView textView = findViewById(R.id.creatorInfo);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

}
